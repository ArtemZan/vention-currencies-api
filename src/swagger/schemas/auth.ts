/**
* @swagger
* components:
*   schemas:
*     SignupBody:
*       title: SignupBody
*       type: object
*       properties:
*         email:
*            type: string
*         password:
*            type: string
*       required:
*         - email
*         - password
*
*     VerifyEmailBody:
*       title: VerifyEmailBody
*       type: object
*       properties:
*         email:
*            type: string
*         code:
*            type: string
*       required:
*         - email
*         - code
*/