import app from "../app";
import swaggerJsdoc from "swagger-jsdoc"
import swaggerUi from "swagger-ui-express"
import dotenv from "dotenv"

const config = dotenv.config().parsed

const options: swaggerUi.JsonObject = {
  definition: {
    openapi: "3.1.0",
    info: {
      title: "Go backend API",
      version: "0.1.0",
      description:
        "This is the backend for the 'go' website",
      license: {
        name: "MIT",
        url: "https://spdx.org/licenses/MIT.html",
      },
      contact: {
        name: "",
        url: "",
        email: "",
      },
    },
    servers: [
      {
        url: config?.BASE_URL,
      },
    ],
  },
  apis: [
    "./src/endpoints/*.ts",
    "./src/swagger/schemas/*.ts",
    "./src/endpoints/*.js",
    "./src/swagger/schemas/*.js"
  ],
};

const specs = swaggerJsdoc(options);

app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs, { explorer: true })
);