import Agenda from "agenda";

export const agenda = new Agenda({
    db: {
        address: "mongodb://127.0.0.1/agenda"
    }
})

agenda
    .on("ready", async () => {
        console.log("Agenda connected")
        await agenda.start()

        console.log("Agenda started")

        agenda.every("6 minutes", "getValue")
    })
    .on("error", () => console.log("Agenda error"))