import Agenda from "agenda";
import sequelize from "../db";
import { HistoryData } from "../models";
import getSymbolValue from "../endpoints/utils/getSymbolValue";
import { agenda } from "./init";


agenda.define("getValue", async () => {
    console.log("agnda processor invoked: getValue")
    const symbol = "ETHUSDT"

    // Deliberately initialized before the request
    const timestamp = new Date().getTime()

    const resp = await getSymbolValue(symbol)

    console.log(resp)

    await HistoryData.create({
        symbol,
        timestamp,
        price: resp.price
    })
})