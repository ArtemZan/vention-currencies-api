import app from "../app";
import db from "../db";
import sha256 from "crypto-js/sha256"
import { User, VerificationCode } from "../models";
import { emailTransporter } from "../email";
import crypto from "crypto"
import { Model, Op } from "sequelize";
import jwt from "jsonwebtoken"

const enum ERROR_CODES {
    EMAIL_TAKEN = "EMAIL_TAKEN",
    INVALID_CODE = "INVALID_CODE",
    WRONG_CREDENTIALS = "WRONG_CREDENTIALS",
    LOGIN_BLOCKED = "LOGIN_BLOCKED",
    EMAIL_ALREADY_VERIFIED = "EMAIL_ALREADY_VERIFIED"
}

/**
* @openapi
* /signup:
*   post:
*       tags: 
*          - Authentication
*       requestBody:
*           required: true
*           content:
*               application/json:
*                   schema:
*                       $ref: '#/components/schemas/SignupBody'
*       responses:
*           200:
*               content: 
*                   application/json:
*                       schema: 
*/
app.post("/signup", async (req, resp) => {
    console.log("Sign up")
    const body = req.body
    const password = sha256(body.password).toString()

    const isUsed = await checkIfEmailUsed(body.email)

    console.log("Is email used: ", isUsed)

    if (isUsed) {
        resp.status(400).send({
            msg: "This email is already taken",
            error: ERROR_CODES.EMAIL_TAKEN
        })

        return
    }

    await User.create({
        email: body.email,
        password
    })

    await sendEmailVerificationCode(body.email)

    resp.sendStatus(200)
})


async function checkIfEmailUsed(email: string) {
    const resp = await User.findOne({ where: { email } })

    return !!resp
}

async function sendEmailVerificationCode(email: string) {
    const code = crypto.randomBytes(8).toString("base64")

    await VerificationCode.create({
        code,
        email
    })

    await emailTransporter.sendMail({
        from: process.env.EMAIL_SENDER,
        to: email,
        subject: "Currencies API - verify your email",
        html: `
            <div style="padding-bottom: 50px;">
                <h2>Click the button below to verify your email</h2>

                <a 
                    href="${process.env.BASE_URL}/verify-email?code=${code}&email=${email}"
                    style="background-color: #446688; border-radius: 5px; padding: 10px; color: white ">

                    Verify email
                </a>
            </div>
        `
    })
}


/**
* @openapi
* /login:
*   post:
*       tags: 
*          - Authentication
*       requestBody:
*           required: true
*           content:
*               application/json:
*                   schema:
*                       $ref: '#/components/schemas/SignupBody'
*       responses:
*           200:
*               content: 
*                   application/json:
*                       schema: 
*/
app.post("/login", async (req, resp) => {
    const body = req.body
    const password = sha256(body.password).toString()

    // To do: fix typization
    const foundUser: any = await User.findOne({
        where: {
            email: body.email,
        }
    })

    if (foundUser.blockedAt) {
        // Check if blocked before less that 10 minutes
        const timeAfterBlock = new Date().getTime() - foundUser.blockedAt
        if(timeAfterBlock < 10 * 60 * 1000)
        {
            resp.status(400).send({
                msg: "The account is blocked for too many incorrect credentials",
                code: ERROR_CODES.LOGIN_BLOCKED,
                data: {
                    timeBeforeUnblock: 10 * 60 * 1000 - timeAfterBlock
                }
            })
            return
        }
    }

    if (!foundUser || foundUser.password !== password) {
        resp.status(400).send({
            msg: "Wrong credentials",
            code: ERROR_CODES.WRONG_CREDENTIALS
        })
    }

    if (!foundUser) {
        return
    }

    if (foundUser.password !== password) {
        const failedLoginAttempts = (foundUser.failedLoginAttempts || 0) + 1
        const update = {
            failedLoginAttempts,
            blockedAt: null
        }

        if (failedLoginAttempts === 5) {
            // To do: fix type
            update.blockedAt = new Date().getTime() as any
        }

        await User.update(update, {
            where: {
                id: foundUser.id
            }
        })

        return
    }

    // to do reset the block
    if(foundUser.blockedAt)
    {
        await User.update({
            blockedAt: null,
            failedLoginAttempts: 0
        }, {
            where: {
                id: foundUser.id
            }
        })
    }

    const jwtBody = {
        email: body.email
    }

    const idToken = jwt.sign(jwtBody, process.env.JWT_SECRET_KEY as string)

    resp.status(200).send({
        idToken
    })
})


/**
* @openapi
* /verify-email:
*   post:
*       tags: 
*          - Authentication
*       requestBody:
*           required: true
*           content:
*               application/json:
*                   schema:
*                       $ref: '#/components/schemas/VerifyEmailBody'
*       responses:
*           200:
*               content: 
*                   application/json:
*                       schema: 
*/
app.post("/verify-email", async (req, resp) => {
    const body = req.body

    const foundUser: any = await VerificationCode.findOne({ where: { email: body.email } })

    if(foundUser?.isEmailVerified){
        resp.status(400).send({
            msg: "The email has already been verified",
            error: ERROR_CODES.EMAIL_ALREADY_VERIFIED
        })

        return
    }

    let foundCodeEntry: any
    
    if(foundUser){
        foundCodeEntry = await VerificationCode.findOne({ where: { email: body.email } })
    }

    if (!foundUser || !foundCodeEntry || foundCodeEntry.code !== body.code) {
        resp.status(400).send({
            msg: "Wrong email or code",
            error: ERROR_CODES.INVALID_CODE
        })

        return
    }

    await foundCodeEntry.destroy()

    await User.update({ isEmailVerified: true }, { where: { email: body.email } })

    resp.sendStatus(200)
})