import https from "https"

export default function getSymbolValue(symbolName: string): Promise<any> {
    return new Promise((resolve, reject) => {
        const currencyReq = https.request(`https://api.binance.com/api/v3/ticker/price?symbol=${symbolName}`, (res) => {
            res.on("data", data => {
                resolve(JSON.parse((data as Buffer).toString()))
            })
        })

        currencyReq.on('error', (e) => {
            reject(e);
        });

        currencyReq.end()
    })
}