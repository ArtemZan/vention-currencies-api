import app from "../app";
import getSymbolValue from "./utils/getSymbolValue";

/**
* @openapi
* /convert:
*   get:
*       tags: 
*          - Convert
*       requestBody:
*           application/json:
*       responses:
*           200:
*               content: 
*                   application/json:
*                       schema: 
*/
app.get("/convert", async (req, resp) => {
    // To do: handle errors

    const BTCUSDTratio = await getSymbolValue("ETHUSDT")

    const ratio = 1 / BTCUSDTratio?.price

    resp.status(200).send(String(ratio))
})