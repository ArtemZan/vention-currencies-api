import createApp from "express"
import dotenv from "dotenv"
import bodyParser from "body-parser"

dotenv.config()

const app = createApp()

app.listen(process.env.PORT, () => {
    console.log("Listening on port: ", process.env.PORT)
})

app.use(bodyParser.json())

export default app
