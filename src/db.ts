import dotenv from "dotenv"
import { Sequelize } from "sequelize"
dotenv.config()

const sequelize = new Sequelize(process.env.DB_NAME as string, process.env.DB_USER as string, process.env.DB_PWD, {
    dialect: "postgres",
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),

});

(async () => {
    try {
        await sequelize.authenticate()
        console.log("Connected to the databse using sequelize")

        await sequelize.sync()
        console.log("Synchronized models with the database")
    }
    catch (e) {
        console.log("Failed to connect to db using sequelize: ", e)
    }
})();

export default sequelize