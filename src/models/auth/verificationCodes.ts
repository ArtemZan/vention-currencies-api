import { DataTypes } from "sequelize";
import sequelize from "../../db";

export const VerificationCode = sequelize.define("VerificationCode", {
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    code: {
        type: DataTypes.STRING,
        allowNull: false
    }
})