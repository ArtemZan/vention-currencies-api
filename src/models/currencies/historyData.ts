import { DataTypes } from "sequelize";
import sequelize from "../../db";

const attributes = {
    symbol: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.STRING,
        allowNull: false
    },
    timestamp: {
        type: DataTypes.DATE,
        defaultValue: null,
        allowNull: false
    }
}

export const HistoryData = sequelize.define("HistoryData", attributes)
export const AveragedHistoryData = sequelize.define("AveragedHistoryData", attributes)