# Currencies API

## For developers

`.env` file template:
```
PORT=4000

DB_USER=""
DB_PWD=""
DB_HOST=""
DB_NAME=""
DB_PORT=

EMAIL_SERVICE=""
EMAIL_SENDER=""
EMAIL_PASSWORD="..."
```

## For users

See the endpoints (swagger) at /api-docs path